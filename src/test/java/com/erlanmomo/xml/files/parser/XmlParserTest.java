package com.erlanmomo.xml.files.parser;

import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class XmlParserTest {
    private static final XmlParser xmlParser = new XmlParser();

    @Test
    public void testSimple() throws ParserConfigurationException, SAXException, IOException {
        List<String> result = xmlParser.parse(getTestFile("test-1.xml"), null, null);

        assertEquals(1, result.size());
        assertEquals("/file-1.txt", result.get(0));
    }

    @Test
    public void testWithDirectories() throws ParserConfigurationException, SAXException, IOException {
        List<String> result = xmlParser.parse(getTestFile("test-2.xml"), null, null);

        assertEquals(1, result.size());
        assertEquals("/dir-1/file-1.java", result.get(0));
    }

    @Test
    public void testWithFileTree() throws ParserConfigurationException, SAXException, IOException {
        List<String> result = xmlParser.parse(getTestFile("test-3.xml"), null, null);

        assertEquals(3, result.size());
        assertEquals("/file-1.txt", result.get(0));
        assertEquals("/dir-1/file-2", result.get(1));
        assertEquals("/dir-1/dir-2/file-5", result.get(2));
    }

    @Test
    public void testSimpleSearch() throws ParserConfigurationException, SAXException, IOException {
        List<String> result = xmlParser.parse(getTestFile("test-3.xml"), "file-2", null);

        assertEquals(1, result.size());
        assertEquals("/dir-1/file-2", result.get(0));
    }

    @Test
    public void testSimpleSearchPattern() throws ParserConfigurationException, SAXException, IOException {
        List<String> result = xmlParser.parse(getTestFile("test-3.xml"), "*.txt", null);

        assertEquals(1, result.size());
        assertEquals("/file-1.txt", result.get(0));
    }

    @Test
    public void testExtSearch() throws ParserConfigurationException, SAXException, IOException {
        List<String> result = xmlParser.parse(getTestFile("test-4.xml"), null, ".*\\.j[a-z|A-Z]+");

        assertEquals(3, result.size());
        assertEquals("/file-8.java", result.get(0));
        assertEquals("/dir-1/file-3.java", result.get(1));
        assertEquals("/dir-1/dir-2/file-9.jsp", result.get(2));
    }

    private File getTestFile(String name) {
        ClassLoader classLoader = getClass().getClassLoader();
        return new File(classLoader.getResource(name).getFile());
    }
}
