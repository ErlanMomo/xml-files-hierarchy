package com.erlanmomo.xml.files;

import com.erlanmomo.xml.files.parser.Parser;
import com.erlanmomo.xml.files.parser.XmlParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        Options options = new Options();

        Option xmlFileOption = new Option("f", "file", true, "Xml file with files hierarchy");
        xmlFileOption.setRequired(true);
        options.addOption(xmlFileOption);

        Option simpleSearchInputOption = new Option("s", "search", true, "Search input string");
        simpleSearchInputOption.setRequired(false);
        options.addOption(simpleSearchInputOption);

        Option extSearchInputOption = new Option("S", "search regex", true, "Search input regex");
        simpleSearchInputOption.setRequired(false);
        options.addOption(extSearchInputOption);

        CommandLineParser commandLineParser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = commandLineParser.parse(options, args);

            String xmlFilePath = cmd.getOptionValue("f");
            String simpleSearchInput = cmd.getOptionValue("s");
            String extSearchInput = cmd.getOptionValue("S");

            Parser parser = new XmlParser();

            List<String> result = parser.parse(
                    xmlFilePath,
                    simpleSearchInput != null ? simpleSearchInput.replace("'", "") : null,
                    extSearchInput != null ? extSearchInput.replace("'", "") : null
            );
            result.forEach(System.out::println);
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            formatter.printHelp("", options);

            System.exit(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
