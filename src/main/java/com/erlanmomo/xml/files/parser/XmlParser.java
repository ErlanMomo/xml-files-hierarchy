package com.erlanmomo.xml.files.parser;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;

public class XmlParser implements Parser {
    private static final String IS_FILE_ATTRIBUTE = "is-file";
    private static final String NAME_ELEMENT = "name";
    private static final String CHILDREN_ELEMENT = "children";
    private static final String CHILD_ELEMENT = "child";

    public List<String> parse(String filePath, String simpleSearch, String extSearch) throws IOException, SAXException, ParserConfigurationException {
        File file = new File(filePath);
        return parse(file, simpleSearch, extSearch);
    }

    public List<String> parse(File file, String simpleSearch, String extSearch) throws IOException, SAXException, ParserConfigurationException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(file);

        document.getDocumentElement().normalize();

        NodeList nodeList = document.getElementsByTagName(document.getDocumentElement().getNodeName());

        Element rootElement = (Element) nodeList.item(0);

        Predicate<String> search = buildSearchPredicate(simpleSearch, extSearch);

        return parseElement(new LinkedList<>(), rootElement, search);
    }

    private static Predicate<String> buildSearchPredicate(String simpleSearch, String extSearch) {
        if (simpleSearch != null && extSearch != null) {
            throw new IllegalArgumentException("Simple and ext search detected. Use either simple or extended");
        }
        if (simpleSearch != null) {
            PathMatcher pathMatcher = FileSystems.getDefault().getPathMatcher("glob:" + simpleSearch);
            return str -> pathMatcher.matches(Path.of(str));
        }
        if (extSearch != null) {
            return str -> str.matches(extSearch);
        }
        return null;
    }

    private static List<String> parseElement(List<String> parent, Element element, Predicate<String> search) {
        boolean isFile = isFile(element);
        String name = getFileName(element);
        if (!isFile) {
            parent.add(name);
            Node node = element.getElementsByTagName(CHILDREN_ELEMENT).item(0);
            NodeList children = ((Element) node).getElementsByTagName(CHILD_ELEMENT);
            List<String> result = new ArrayList<>();
            for (int i = 0; i < children.getLength(); i++) {
                Node childNode = children.item(i);
                if (childNode.getParentNode().equals(node)) {
                    result.addAll(parseElement(parent, (Element) childNode, search));
                }
            }
            parent.remove(parent.size() - 1);
            return result;
        } else {
            List<String> result = new LinkedList<>();
            String filePath = String.join("/", parent) + "/" + name;
            if (search == null || search.test(name)) {
                result.add(filePath);
            }
            return result;
        }
    }

    private static boolean isFile(Element element) {
        return Boolean.TRUE.toString().equalsIgnoreCase(element.getAttribute(IS_FILE_ATTRIBUTE));
    }

    private static String getFileName(Element element) {
        String name = element.getElementsByTagName(NAME_ELEMENT).item(0).getTextContent();
        return name.equals("/") ? "" : name;
    }
}
