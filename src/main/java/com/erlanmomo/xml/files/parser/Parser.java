package com.erlanmomo.xml.files.parser;

import java.io.File;
import java.util.List;

public interface Parser {
    List<String> parse(String filePath, String simpleSearch, String extSearch) throws Exception;
    List<String> parse(File file, String simpleSearch, String extSearch) throws Exception;
}
